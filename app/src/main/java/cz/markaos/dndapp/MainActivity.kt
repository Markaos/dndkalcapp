package cz.markaos.dndapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import cz.markaos.dndkalc.CharacterTemplate
import cz.markaos.dndkalc.Class
import cz.markaos.dndkalc.Race
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_stat.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinner_race.adapter = ArrayAdapter<String>(this, R.layout.item_spinner_string, Race.values().map { it.humanName.capitalize() })
        spinner_class.adapter = ArrayAdapter<String>(this, R.layout.item_spinner_string, Class.values().map { it.humanName.capitalize() })

        val spinnerListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                dataChanged()
            }
        }

        spinner_race.onItemSelectedListener = spinnerListener
        spinner_class.onItemSelectedListener = spinnerListener
    }

    private fun dataChanged() {
        val race = Race.values()[spinner_race.selectedItemPosition]
        val clazz = Class.values()[spinner_class.selectedItemPosition]
        val character = CharacterTemplate(race, clazz)
        val myRange = character.statRanges

        strength_item.statRange = myRange.strength
        agility_item.statRange = myRange.agility
        constitution_item.statRange = myRange.constitution
        intelligence_item.statRange = myRange.intelligence
        charisma_item.statRange = myRange.charisma
    }
}
