package cz.markaos.dndapp

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.SeekBar
import cz.markaos.dndkalc.stats.Range
import kotlinx.android.synthetic.main.item_stat.view.*

class StatView : LinearLayout {

    var statRange: Range = Range(0, 0)
        set(value) {
            field = value
            changeData()
        }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val style = context.theme.obtainStyledAttributes(attrs, R.styleable.StatView, 0, 0)
        val statName = style.getText(R.styleable.StatView_name) as String? ?: "brm brm"
        name.text = statName
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.item_stat, this)
        val seekbarListener = object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                changeData()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        }

        roll_bar.setOnSeekBarChangeListener(seekbarListener)
    }

    private fun changeData() {
        roll_result.text = statRange.asDiceRoll()
        roll_bar.tag = statRange.diceCount()
        roll_bar.max = statRange.diceCount() * 6 - roll_bar.tag as Int
        roll_value_raw.text = (roll_bar.progress + roll_bar.tag as Int).toString()
        roll_bonus.text = (roll_bar.progress + roll_bar.tag as Int).let {
            "${statRange.applyDiceRoll(it).level} (${statRange.applyDiceRoll(it).modifier.let { mod -> 
                when {
                    mod > 0 -> "+"
                    // Minus sign is added automatically and zero shouldn't have a sign
                    else   -> ""
                } + mod
            }})"
        }
    }
}